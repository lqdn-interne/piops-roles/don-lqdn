don-lqdn
=========

Un rôle Ansible pour l'installation du site de don de La Quadrature du Net

Requirements
------------

Toutes les dépendences sont installées et configurées par ce rôle.

Role Variables
--------------

Voir le fichiers `vars/main.yml`


Dependencies
------------

N/A

Example Playbook
----------------

    - name: Site de don
      hosts: don.lqdn.fr
      remote_user: root
      vars_files:
        - group_vars/don/don.yml
      roles:
        - security-lqdn
        - ansible-role-certbot
        - ansible-role-nginx
        - ansible-role-firewall
        - ansible-node-exporter
        - don-lqdn
      tags:
        - don
        - production

License
-------

GPLv3

Author Information
------------------

Écrit en 2021 par nono ( np@laquadrature.net ) pour La Quadrature Du Net
